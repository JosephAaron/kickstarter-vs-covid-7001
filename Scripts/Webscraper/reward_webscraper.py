from html.parser import HTMLParser
import urllib
import urllib.request
import pandas as pd
from tqdm import tqdm
import pathlib
import ast

class LinkParser(HTMLParser):
    def __init__(self):
        super().__init__()
        self._recording_amount = 0
        self._recording_backers = 0
        self._in_pledge = 0
        self._reward_amounts = ()
        self._reward_backers = ()
        

    def handle_starttag(self, tag, attr):
        if tag not in ['span', 'h2']:
            return
        if tag == 'h2':
            for name, value in attr:
                if name == 'class' and value == 'pledge__amount':
                    self._in_pledge = 1
        elif self._in_pledge == 1:
            for name, value in attr:
                if name == 'class': 
                    if value == 'money':
                        self._recording_amount = 1
                    elif value == 'pledge__backer-count' or value == 'block pledge__backer-count':
                        self._recording_backers = 1

    def handle_data(self, data):
        if self._recording_amount:
            self._reward_amounts += (data,)
            self._recording_amount = 0
        if self._recording_backers:
            self._reward_backers += (data,)
            self._recording_backers = 0
            self._in_pledge = 0
    
    def get_rewards(self):
        return [self._reward_amounts, self._reward_backers]

def find_rewards(url):
    f = urllib.request.urlopen(url)
    text = f.read()
    f.close()
    parser = LinkParser()
    parser.feed(str(text))
    return parser.get_rewards()

def get_info(filename):
    f = pd.read_csv(filename, usecols=['id','urls'], lineterminator='\n')
    f['urls'] = f['urls'].apply(ast.literal_eval)
    return f

def main():
    datafiles = ['../Data/Filtered_Data_v2.csv']
    file_num = 0
    start_index = 20000
    row_save = 5
    att_err = 0
    url_err = 0
    oth_err = 0
    for datafile in datafiles:
        links = get_info(datafile)
        reward_info = []
        row_count = 1
        #Create new CSV with header, only if it doesn't exist already. Else append to pre-existing CSV
        if not pathlib.Path('reward_output{0}.csv'.format(file_num)).exists():
            reward_info_df = pd.DataFrame(columns=['id','rewards'])
            reward_info_df.to_csv('reward_output{0}.csv'.format(file_num), mode='w')
        else:
            pre_existing = pd.read_csv('reward_output{0}.csv'.format(file_num), usecols=['id'], lineterminator='\n')
            last_id = int(pre_existing['id'].tail(1))
            last_scraped_index = int(links.index[links['id'] == last_id][0]+1)
            start_index = last_scraped_index
        for key, values in tqdm(links.iterrows()):
            if key < start_index:
                continue
            reward_info_specific = {}
            try:
                reward_info_specific['id'] = values['id']
                reward_info_specific['rewards'] = find_rewards(values['urls']['web']['rewards'])
            except AttributeError:
                print("ID or reward not valid - skipping")
                att_err += 1
                continue
            except urllib.error.HTTPError:
                print("URL not valid - skipping")
                url_err += 1
                continue
            except Exception:
                print("Other error - skipping")
                oth_err += 1
                continue
            reward_info.append(reward_info_specific)
            if row_count % row_save == 0: #Append to CSV every 5 reads
                reward_info_df = pd.DataFrame(reward_info)
                reward_info_df.to_csv('reward_output{0}.csv'.format(file_num), mode='a', header=None)
                reward_info = []
            print(values['id'], f"Skipped: {att_err} (Attr.), {url_err} (URL), {oth_err} (Other)")
            row_count += 1
        reward_info_df = pd.DataFrame(reward_info) #Append remaining to CSV
        reward_info_df.to_csv('reward_output{0}.csv'.format(file_num), mode='a', header=None)
        file_num += 1

if __name__ == "__main__":
    main()