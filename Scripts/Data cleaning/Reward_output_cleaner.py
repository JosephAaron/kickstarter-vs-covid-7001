import csv
import ast
import pandas as pd
from tqdm import tqdm

def clean_data():
    cleaned_costs = {}
    cleaned_backers = {}
    for datafile in ["../../Data/Kickstarter/reward_output_1-20000.csv",
                     "../../Data/Kickstarter/reward_output_20000-36633.csv"]:
        with open(datafile) as file:
            reader = csv.DictReader(file)
            count = 0
            for row in tqdm(reader):
                ks_id = int(row["id"])
                costs, backers = ast.literal_eval(row["rewards"])
                
                cleaned_costs[ks_id] = []
                cleaned_backers[ks_id] = []
                for cost in costs:
                    if "\\xc2\\xa3" in cost:
                        cleaned_costs[ks_id].append(int(cost[8:].replace(",","")))
                    elif "\\xc2\\xa5" in cost:
                        cleaned_costs[ks_id].append(int(cost[8:].replace(",","")))
                    elif "\\xe2\\x82\\xac" in cost:
                        cleaned_costs[ks_id].append(int(cost[12:].replace(",","")))
                    else:
                        cleaned_costs[ks_id].append(int(cost.split(" ")[1].replace(",","")))
                for backer in backers:
                    cleaned_backers[ks_id].append(int(backer.split(" ")[0][2:].replace(",","")))
                count += 1
                if count == 20000:
                     break
    return cleaned_costs, cleaned_backers

def write_cleaned_rewards(costs, backers):
    reward_info_df = pd.DataFrame(columns=['id','reward_costs','reward_backers'])
    for i in tqdm(costs):
        reward_info_df = reward_info_df.append({'id': i, 'reward_costs': costs[i], 'reward_backers': backers[i]}, ignore_index=True)
    reward_info_df.to_csv('Cleaned_Reward_Data.csv', mode='w')

costs, backers = clean_data()
write_cleaned_rewards(costs, backers)
