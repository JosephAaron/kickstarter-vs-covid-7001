print(getwd())
library(lubridate)
library(plyr)
setwd("./Inputs/Kickstarter_2020-08-13T03_20_17_470Z (1)")

data_files_list<-list.files(path="./")
full_data_set<-data.frame()
print(getwd())


for (i in 1:length(data_files_list)){
    temp_data<-read.csv(paste("./",data_files_list[i], sep=""))
    temp_data$created_at<-as_datetime(temp_data$created_at)
    filt1<-temp_data$created_at>=ymd("2019-01-01")
    temp_data_filt<-temp_data[filt1,]
    full_data_set<-rbind.fill(full_data_set,temp_data_filt)
    
}


nrow(full_data_set)
full_data_set$state_changed_at<-as_datetime(full_data_set$state_changed_at)
write.csv(full_data_set,"../Filtered_Data.csv")
full_data_set$created_at




