library(readr)
library(dplyr)
library(tidyverse)
library(ROCR)
library(caret)
data = read_csv("./Data/Kickstarter/Cleaned_KS_Data_Final.csv")

#Filter data and remove MCAR campaigns (from webscrape errors)
data$created_at = as.Date(data$created_at,format = "%d/%m/%Y")
data = subset(data, created_at < "2020-03-13" & country == "USA") #Adjust this for pre/post COVID
data = data[data$state_corrected!="live",]
data = data[complete.cases(data),]
data = data[data$reward_costs!="[]",]

#Extract cost info
for (i in 1:nrow(data)) {
  costs = as.numeric(unlist(strsplit(substr(data$reward_costs[[i]],2,nchar(data$reward_costs[[i]])-1), ", ")))*data$fx_rate[[i]]
  costs_quantiles = quantile(costs, na.rm = TRUE)
  data$min[[i]] = costs_quantiles[1]
  data$q1[[i]] = costs_quantiles[2]
  data$median[[i]] = costs_quantiles[3]
  data$q3[[i]] = costs_quantiles[4]
  data$max[[i]] = costs_quantiles[5]
  data$numtiers[[i]] = length(costs)
  data$mean[[i]] = mean(costs)
}

#Format
data$state_binary = as.factor(data$state_corrected)
data$state_binary = as.numeric(data$state_binary)
data[data$state_corrected=="successful",]$state_binary = 1
data[data$state_corrected=="failed",]$state_binary = 0

#Split data
indexes = sample(1:nrow(data), size=0.70*nrow(data))
train.data = data[indexes,]
test.data = data[-indexes,]

#Logistic regression (numtiers vs state)
logfit<-glm(state_binary~numtiers,data=train.data,family=binomial)
summary(logfit)
newamh<-seq(0,150)
predprobs<-predict(logfit,data.frame(numtiers=newamh),type="response")
plot(train.data$numtiers,train.data$state_binary,main="State vs numtiers (Post-COVID)", xlab="Number of tiers",ylab="State (1 = success)",col=rgb(0,0,1,0.5))
lines(newamh,predprobs,col="black",lty=1)

#Evaluate on test set
probs<-predict(logfit,data.frame(numtiers=test.data$numtiers),type="response")
probs<-round(probs)
probs = as.factor(probs)
test.data$state_corrected = as.factor(test.data$state_binary)
confusionMatrix(data=probs, reference=test.data$state_corrected, positive='1')
