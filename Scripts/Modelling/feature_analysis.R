library(readr)
library(caret)
library(randomForest)
library(dplyr)
data = read_csv("./Data/Kickstarter/Cleaned_KS_Data_Final.csv")

#Filter data and remove MCAR campaigns (from webscrape errors)
data$created_at = as.Date(data$created_at,format = "%d/%m/%Y")
data = subset(data, created_at < "2020-03-13" & country == "USA") #Adjust this for pre/post COVID
data = data[data$state_corrected!="live",]
data = data[complete.cases(data),]
data = data[data$reward_costs!="[]",]

#Extract cost info
for (i in 1:nrow(data)) {
  costs = as.numeric(unlist(strsplit(substr(data$reward_costs[[i]],2,nchar(data$reward_costs[[i]])-1), ", ")))*data$fx_rate[[i]]
  costs_quantiles = quantile(costs, na.rm = TRUE)
  data$min[[i]] = costs_quantiles[1]
  data$q1[[i]] = costs_quantiles[2]
  data$median[[i]] = costs_quantiles[3]
  data$q3[[i]] = costs_quantiles[4]
  data$max[[i]] = costs_quantiles[5]
  data$numtiers[[i]] = length(costs)
  data$mean[[i]] = mean(costs)
}

#Format
data$goal = data$goal*data$fx_rate

#Define functions
to.factors = function(df, variables){
  for (variable in variables) {
    df[[variable]] = as.factor(df[[variable]])
  }
  return(df)
}

scale.features = function(df, variables){
  for (variable in variables) {
    df[[variable]] = scale(df[[variable]], center=T, scale=T)
  }
  return(df)
}

#Remove unwanted variables
data$X=NULL
data$blurb=NULL
data$country_displayable_name=NULL
data$currency=NULL
data$currency_symbol=NULL
data$currency_trailing_code=NULL
data$current_currency=NULL
data$disable_communication=NULL
data$id=NULL
data$is_starrable=NULL
data$created_at=NULL
data$name=NULL
data$photo=NULL
data$reward_backers=NULL
data$reward_costs=NULL
data$source_url=NULL
data$state=NULL
data$state_changed_at=NULL
data$usd_type=NULL
data$backers_count=NULL
data$converted_pledged_amount=NULL
data$index=NULL
data$pledged=NULL
data$fx_rate=NULL
data$usd_pledged=NULL
data$ratioP2Glog=NULL
data$url_main=NULL
data$url_rewards=NULL
data$static_usd_rate=NULL
data$deadline=NULL
data$launched_at=NULL
data$main_category=NULL
data$sub_category=NULL
data$static_usd_rate=NULL
data$spotlight=NULL
data$staff_pick=NULL
data$country=NULL

#Sample data
#data = data[sample(nrow(data), 10000), ]

#Normalize variables
numeric.vars = c("goal", "ratioP2G","campaign_length_days","numtiers","min","max","median","mean","q1","q3")
data = scale.features(data, numeric.vars)

#Factor variables
categorical.vars = c("state_corrected")
data = to.factors(data, categorical.vars)

#Split data
indexes = sample(1:nrow(data), size=0.75*nrow(data))
train.data = data[indexes,]
test.data = data[-indexes,]

#Feature selection function
run.feature.selection = function(num.iters=20, feature.vars, class.var){
  set.seed(10)
  variable.sizes = 1:10
  control = rfeControl(functions = rfFuncs, method = "cv", verbose = FALSE, returnResamp = "all", number = num.iters)
  results.rfe = rfe(x = feature.vars, y = class.var, sizes = variable.sizes, rfeControl = control)
  return(results.rfe)
}

#Run feature selection algorithm
rfe.results1 = run.feature.selection(feature.vars = train.data[, c(1,4:ncol(train.data))], class.var = pull(train.data, state_corrected))
rfe.results1
varImp(rfe.results1)

rfe.results2 = run.feature.selection(feature.vars = train.data[, c(1,4:ncol(train.data))], class.var = pull(train.data, ratioP2G))
rfe.results2
varImp(rfe.results2)