*Note:* All sensitive data (assessment documents and datasets) has been removed from this repository.

# Kickstarter vs COVID-19
Using webscraped Kickstarter data (~207,000 campaigns) to investigate the effect of COVID-19 on the campaign success rates. Group project for DATA7001 in semester 2 2020.


## Goals
1. Discover key variables of Kickstarter campaign success
2. Determine differences between campaign performance and trends for before/during COVID
3. Build and evaluate predictive models for before/during COVID


## Data
- WebRobots Kickstarter dataset
	- Horizontally fragmented CSVs with 39 variables and ~207,000 observations
	- Reduced to ~37,000 observations - Equally sized groups before/during COVID
	- Unclean (many aggregated attributes)
	- Available at https://webrobots.io/kickstarter-datasets/
- Kickstarter rewards data
	- Data pertaining to reward tier information on the campaigns in the Kickstarter dataset
	- Web scraped using custom-built scraper
- Our World in Data COVID-19 dataset
	- CSV with 41 variables and ~46,000 observations
	- Indexed by country and date
	- Relatively clean, but plenty of missing data
	- Available at https://ourworldindata.org/coronavirus-source-data


## Outcomes
EDA revealed some interesting patterns. For example, the day on which a campaign is launched seems to be associated with success.
![launchday_EDA](/Output/launchday_EDA.png)

Another interesting pattern is the effect of campaign length on success.
![length_EDA](/Output/length_EDA.png)

Attempts were made to cluster groups by ratio of amount pledged to backer count (i.e. separating high-pledging users from low-pledging users) which would be cross-referenced with campaign category to determine pledge behaviour differences. However, this was a long shot and unsuccessful.
![k-means_mosaic_categories](/Output/k-means_mosaic_categories.png)

Tree-based feature analysis was performed to determine tentative order of importance of predictors.

For predictive modelling, kNN classification was used for the most relevant predictors, but this didn't perform well.
![knn_performance](/Output/knn_performance.png)

A logistic regression model on the number of reward tiers performed better than a Bayes optimal classifier (Imbalanced classes - Use PPV).
![logreg_performance](/Output/logreg_performance.png)

Random forests turned out to be the best predictive model for both before/during COVID.
![rf_performance](/Output/rf_performance.png)

From this model, we could determine how predictor importance changed with COVID.
![rf_variable_importance](/Output/rf_variable_importance.png)