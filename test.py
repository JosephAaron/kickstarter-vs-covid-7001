import matplotlib.pyplot as plt

a = plt.imread('Support clusters pre-COVID in the US.png')
b = plt.imread('Category support pre-COVID in the US.png')
c = plt.imread('Support clusters post-COVID in the US.png')
d = plt.imread('Category support post-COVID in the US.png')

plt.figure(figsize=(100,100))
plt.subplot(2,2,1); plt.imshow(a); plt.axis('off')
plt.subplot(2,2,3); plt.imshow(b); plt.axis('off')
plt.subplot(2,2,2); plt.imshow(c); plt.axis('off')
plt.subplot(2,2,4); plt.imshow(d); plt.axis('off')
plt.subplots_adjust(wspace=0, hspace=-0.25)